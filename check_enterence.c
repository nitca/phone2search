#include "check_enterence.h"


int lenstr(char str[])
{
	int i = 0;
	for(;str[i]; ++i);
	return i;
}

//checking if user enterence has only number
bool find_number(char symbol)
{
	char numbers[] = "1234567890";
	for(char i = 0; numbers[i]; ++i)
	{
		if(symbol == numbers[i])
			return false;
	}
	printf("Use only numbers!\n");
	return true;
}

//full user enterence
bool check_phone_number(char number[])
{
	if(lenstr(number) != 11)
	{
		printf("The phonenumber must be in 11 numbers!\n");
		return true;
	}
	for(int i = 0;number[i];++i)
	{
		if(find_number(number[i]))
			return true;
	}
	return false;
}
