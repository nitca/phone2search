# phone2search

### Simple cross platform programm for get web search string of your phone number.

### For example: 
#### from string:
```
71234567890
```

#### you can get: 
```
"71234567890" | "81234567890" | "7 123 4567890" | "8 123 4567890" | "7 123 456 78 90" | "8 123 456 78 90" | "7.1234567890" | "8(123)4567890" | "(123)4567890" |"7(123)456-78-90" | "8(123)456-78-90"
```
## How to build with make tool:

### make build -- build the project

### make debag -- build the project for debag

### make clean -- delete all "*.o" files

### make help -- help menu


## If you don't have make:

### You can input this command for build:
```
gcc check_enterence.c work_with_string.c main.c
```

## How to use:

```
phone2number <phone number>
``` 
